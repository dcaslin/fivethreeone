'use strict';

function testMe() {
	// var ps = new PlateSolution(135, 134, [2.5, 2.5], 45);

	var plates = {
		"5" : 1,
		"10" : 3,
		"25" : 0,
		"35" : 1,
		"45" : 2,
		//"2_5" : 1
		//"1" : null
	};
	var pt = new PlateTable(186, 45, plates);
	var result = pt.getSolution(185);

	console.log(result);
}

function PlateSolution(goal, actual, plates, bar) {
	this.goal = goal;
	this.actual = actual;
	this.plates = plates;
	this.bar = bar;
}

PlateSolution.prototype.print = function() {
	var s = "";
	s += "";
	s += ("Goal: " + this.goal);
	s += (", Actual: " + this.actual);
	s += (", Plates: ");
	var cntr;
	for (cntr = 0; cntr < this.plates.length; cntr++) {
		s += this.plates[cntr] + " ";
	}
	return s;
};

function PlateTable(maxValue, barWeight, plateData) {
	function sortNumber(a,b) {
	    return a - b;
	}
	
	function getOrderedLabels(iterMe, castToInt) {
		var labels = [];
		for ( var label in iterMe) {
			if (iterMe.hasOwnProperty(label)) {
				var pushMe = parseInt(label, 10);
				labels.push(pushMe);
			}
		}
		labels.sort(sortNumber);
		return labels;

	}

	function isInt(n) {
		return n % 1 === 0;
	}

	
	//checkMe = [5], platesAndCounts = Object {5: 1, 10: 3, 35: 1, 45: 2}
	function checkPlates(checkMe, platesAndCounts) {
		var realTotals = {};
		
		var cntr;
		for (cntr=0; cntr<checkMe.length; cntr++){
			var i = checkMe[cntr];
			var cnt = realTotals[i]
			if (cnt == null)
				cnt = 0;
			cnt++;
			realTotals[i] = cnt;
		}
		
		for ( var key in realTotals) {
			if (realTotals.hasOwnProperty(key)) {
				var total = realTotals[key];
				var max = platesAndCounts[key];
				if (total > max)
					return false;
			}
		}
		return true;
	}

	function buildTable(maxValue, platesAndCounts) {
		maxValue++;
		var plates = getOrderedLabels(platesAndCounts, true);
		var mins = new Array(maxValue);
		mins[0] = 0;
		var usedPlates = {};
		usedPlates[0] = [];
		var i, j;
		for (i = 0; i < maxValue; i++) {

			for (j = 0; j < plates.length; j++) {
				var plateWeight = plates[j];
				var totalWeight = 2 * plateWeight;
				if (totalWeight > i)
					continue;

				var lastIndex = i - totalWeight;
				if (lastIndex < 0)
					continue;
				var oldMins = mins[lastIndex];
				if (oldMins == null)
					continue;
				var newMins = oldMins + 1;
				var nextB = mins[i];

				if (nextB == null || newMins < nextB) {
					var oldPlates = usedPlates[lastIndex];
					var newPlates = new Array();
					var pC;
					for (pC=0; pC<oldPlates.length; pC++)
						newPlates.push(oldPlates[pC]);
					
					newPlates.push(plateWeight);
					if (checkPlates(newPlates, platesAndCounts)) {
						usedPlates[i] = newPlates;
						mins[i] = newMins;
					}
				}

			}
		}
		return usedPlates;

	}

	this.multiplier = 1;
	var aPlateVals = [];
	var plateCntLookup = {};
	this.multiplier = 1;
	for ( var label in plateData) {
		if (plateData.hasOwnProperty(label)) {
			var count = plateData[label];
			if (count == null)
				continue;
			if (count == 0)
				continue;
			var label = label.replace("_", ".");
			var weight = parseFloat(label);
			aPlateVals.push(weight);
			plateCntLookup[weight] = count;

			var mult;
			if (isInt(weight))
				mult = 1;
			else if (isInt(weight * 2))
				mult = 2;
			else if (isInt(weight * 4))
				mult = 4;
			else
				throw "Multiplier can't be higher than 4";
			if (mult > this.multiplier)
				this.multiplier = mult;
		}
	}
	console.log("Multipler is " + this.multiplier);
	this.barWeight = barWeight * this.multiplier;
	maxValue = maxValue * this.multiplier;

	var modPlateCntLookup = {};
	for ( var key in plateCntLookup) {
		if (plateCntLookup.hasOwnProperty(key)) {
			var key2 = this.multiplier * key;
			modPlateCntLookup[key2] = plateCntLookup[key];
		}
	}
	this.table = buildTable(maxValue, modPlateCntLookup);
	console.log("x");

}

PlateTable.prototype.getSolution = function(goal) {
	goal = goal * this.multiplier;
	var value = goal;
	value = value - this.barWeight;
	if (value < 0)
		return null;
	var spot = value;
	spot++;
	var selection = null;

	while (selection == null) {
		spot--;
		selection = this.table[spot];
		if (spot == 0)
			break;
	}

	if (selection == null)
		return null;
	var total = this.barWeight;

	var newSelection = [];
	var cntr = 0;
	for (cntr = 0; cntr < selection.length; cntr++) {
		var i = selection[cntr];
		newSelection.push(i / this.multiplier);
		total += 2 * i;
	}

	var returnMe = new PlateSolution(goal / this.multiplier, total / this.multiplier, newSelection, this.barWeight / this.multiplier);
	return returnMe;

}