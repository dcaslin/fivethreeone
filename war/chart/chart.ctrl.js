function drawChart(excId, excName, diary, divId, chartType) {
    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Month');
    data.addColumn('number', "1RM");
    data.addColumn('number', "Weight");
    var cntr = 0;
    for (; cntr < diary.length; cntr++) {
        var entry = diary[cntr];
        if (entry.excId == excId) {
            data.addRow([moment(entry.date).toDate(), entry.oneRm, entry.weight]);
        }
    }
    var chart = null;
    var options = null;
    if ("scatter" == chartType) {
        options = {
            'title': excName,
            trendlines: {
                0: {
                    //type: 'exponential'
                },
                1: {
                    //type: 'exponential'
                }
            }
        };
        chart = new google.visualization.ScatterChart(document.getElementById(divId));
    }
    else {
        options = {
            'title': excName,
            //curveType: "function"
        };

        chart = new google.visualization.LineChart(document.getElementById(divId));
    }
    chart.draw(data, options);
}

angular.module('five31app').controller(
    "ChartController", ['$scope', function ($scope) {
        $scope.chartType = "scatter";
        $scope.drawChart = function () {
            console.log("Draw chart");
            drawChart("dl", "DeadLift", $scope.settings.diary, "dlChart", $scope.chartType);
            drawChart("sq", "Squat", $scope.settings.diary, "sqChart", $scope.chartType);
            drawChart("bp", "Bench Press", $scope.settings.diary, "bpChart", $scope.chartType);
            drawChart("ohp", "Overhead Press", $scope.settings.diary, "ohpChart", $scope.chartType);
        };
        $scope.drawChart();
        $(window).resize(function () {
            if (ON_CHART)
                $scope.drawChart();
        });
    }]);