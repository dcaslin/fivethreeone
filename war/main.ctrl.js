var ON_CHART = false;
var SOLUTION_TABLE = null;

function buildPlateInfo($scope) {
	var plateReference = $scope.plates[$scope.settings.info.units];
	var plateData = {
		bar : plateReference.bar,
		plates : []
	};
	var platesAvail = $scope.settings.plates[$scope.settings.info.units];

	for ( var label in platesAvail) {
		if (platesAvail.hasOwnProperty(label)) {
			var count = platesAvail[label];
			label = label.replace("_", ".");
			var weight = parseFloat(label);
			var cntr;
			for (cntr = 0; cntr < count; cntr++) {
				plateData.plates.push(weight);
			}
		}
	}
	plateData.plates.sort(reverseSortNumber);
	return plateData;
}

function totalWeights(bar, plates) {
	var returnMe = bar;
	var cntr;
	for (cntr = 0; cntr < plates.length; cntr++) {
		returnMe += 2 * plates[cntr];
	}
	return returnMe;
}

function calcPlatesForSetList($scope, setList){
	var cntr;
	for (cntr = 0; cntr < setList.sets.length; cntr++) {
        var cSet = setList.sets[cntr];
		var weight = cSet.weight;
        //don't calc if we already did
        if (cSet.plates!=null || cSet.msg!=null) continue;
        var plateSolution = SOLUTION_TABLE.getSolution(weight);
		if (plateSolution) {
            cSet.plates = plateSolution.plates;
            cSet.weight = plateSolution.actual;
		} else {
            cSet.msg = "You need more weights!";
		}
	}
}

function calcPlatesForDay($scope, day, deloadWeek){
	calcPlatesForSetList($scope, day.warmup);
	calcPlatesForSetList($scope, day.primary);
	if (!deloadWeek)
		calcPlatesForSetList($scope, day.assistance);
}

function calcPlatesForWeek($scope, week){
	var cntr;
	for (cntr=0; cntr<week.days.length; cntr++){
		calcPlatesForDay($scope, week.days[cntr], week.weekNum==4);
	}
}

function calcPlatesForWorkout($scope, workout){
	var cntr;
	for (cntr=0; cntr<workout.weeks.length; cntr++){
		calcPlatesForWeek($scope, workout.weeks[cntr]);
	}
	console.log("Done calc.");
}


angular.module('five31app').controller("MainController", 
		['$interval', '$scope', '$rootScope', '$location', 'UserService', 'WorkoutSet', 'WorkoutSetList','WorkoutDay','WorkoutWeek','Workout', function($interval, $scope, $rootScope, $location, UserService, WorkoutSet, WorkoutSetList, WorkoutDay, WorkoutWeek, Workout) {

	$scope.changeStyle = function(name, newCss) {
		var curTheme = $('#csstheme').attr('href');
		if (curTheme != newCss) {
			console.log(curTheme + "->" + newCss);
			$('#csstheme').attr('href', newCss);
			localStorage.setItem("csstheme", newCss);
			localStorage.setItem("cssname", name);
			$scope.cssName = name;
			$scope.cssTheme = newCss;
		}
	};
    $scope.addDiaryEntry = function (date, weekNum, excId, reps, weight) {
        var calcOneRm = $scope.oneRm(weight, reps);
        var diary = $scope.settings.diary;
        diary.push({
            date: date.toISOString(),
            week: weekNum,
            excId: excId,
            reps: reps,
            weight: weight,
            oneRm: calcOneRm
        });
        $scope.save(false);
    };

	$scope.selectSnapshotUrl = function(){
        var selectMe = $("#snapshotUrl")[0];
		selectMe.focus();
        selectMe.select();
	};
	$scope.logon = function(){
		UserService.getLogonUrl().then(function (url){
			if (url!=null)
				window.location.href = url;
		});
	};
	$scope.loggedOn = false;
	$scope.user = "";
	$scope.cloudLoad = function(){
		UserService.getUserData().then(function (result){
			if (result!=null){
				var data = JSON.parse(result);
				if (data.data){
					$scope.settings = data.data;
					console.log("Cloud loaded: true");
				}
				
			}
			
		});
		
	};
	$scope.checkLogon = function(){
		console.log("Checking logon");
		UserService.isLoggedOn().then(function (name){
			if (name!=null){
				$scope.loggedOn = true;
				$scope.user = name;
			}
			else{
				$scope.loggedOn = false;
			}
			console.log("logged on: "+$scope.loggedOn);
			$scope.cloudLoad();
		});
	};
	$scope.checkLogon();
	$scope.logoff = function(){
		UserService.getLogoffUrl().then(function (url){
			if (url!=null)
				window.location.href = url;
		});
		 
	};
	
	$scope.cloudSave = function(){
		UserService.setUserData($scope.settings).then(function (result){
			console.log("setUserData: "+result);
			$scope.dirty = false;
		});
		
	};
    $scope.snapshotIncludeDiary = true;
    $scope.updateSnapshotUrl = function(includeDiary){
        //deep copy $scope
        $scope.snapshotIncludeDiary = includeDiary;
        var localSettings = JSON.parse(JSON.stringify($scope.settings));
        if (!includeDiary){
            localSettings.diary = {};
        }
        var compressed = LZString.compressToEncodedURIComponent(JSON.stringify(localSettings));
        var url = getAbsUrl();
        url = url + "?k=" + compressed;
        $scope.snapshotUrl = url;
    };
	$scope.showJs = function() {
        $scope.updateSnapshotUrl($scope.snapshotIncludeDiary);
		$('#urlModal').modal();
	};
	$scope.oneRm = function(w, r) {
		//console.log("OneRm: "+w+","+r);
		if (!r)
			r = 0;
		if (!w)
			w = 0;
		if (r == 0) {
			return 0;
		} else if (r == 1) {
			return w;
		} else {
			var val = (w * (1 + (r / 30)));
			val = Math.floor(val);
			return val;
		}
	};
	var cacheTheme = localStorage.getItem("csstheme");
	var cacheName = localStorage.getItem("cssname");
	if (cacheTheme) {
		$scope.changeStyle(cacheName, cacheTheme);
	} else {
		$scope.cssName = "";
		$scope.cssTheme = $('#csstheme').attr('href');
	}
	
	$rootScope.$on("$routeChangeSuccess", function(event, next, current) {
		if (!next.redirectTo) {
			if (next.loadedTemplateUrl=="chart/chart.html"){
				ON_CHART = true;
			}
			else{
				ON_CHART = false;
			}
			console.log("ga -> " + next.loadedTemplateUrl);
			window.ga('send', 'pageview', { page: next.loadedTemplateUrl});
		}
	});
	
	$scope.exc = loadExc();
	$scope.save = function(resetPlan) {
		if (resetPlan){
			$scope.plan = null;
			$scope.settings.liftStatus = getDefaultLiftStatus();
			localStorage.removeItem("days");
			localStorage.removeItem("week");
		}
		localStorage.setItem("531settings", JSON.stringify($scope.settings));
		if ($scope.loggedOn){
			$scope.dirty = true;
		}
	};
	$scope.loadSettings = function() {
		var finalSettings = null;
		// try the query params first
		var urlParams = queryStringToJSON();
		var uSettings = {};
		if (urlParams.k) {
			try {
				var decomp = LZString.decompressFromEncodedURIComponent(urlParams.k);
				uSettings = JSON.parse(decomp);
				$location.search("");
			} catch (exc) {
				// ignore
			}
		}
		// if no query params then try the local storage
		if (Object.keys(uSettings).length == 0) {
			var sJson = localStorage.getItem("531settings");
			var jSettings = {};
			if (sJson) {
				try {
					jSettings = JSON.parse(sJson);
				} catch (exc) {
					// ignore
				}
			}
			finalSettings = angular.extend({}, getDefault(), jSettings);
		} else {
			var finalSettings = angular.extend({}, getDefault(), uSettings);
			$scope.settings = finalSettings;
			$scope.save(true);
			window.location.href = getAbsUrl();
		}
		$scope.settings = finalSettings;
		//fix any dates that were strings
		var cntr;
		for (cntr=0;cntr<$scope.settings.diary.length; cntr++){
			$scope.settings.diary[cntr].date = moment($scope.settings.diary[cntr].date).toDate();
		};
	};
	
	$scope.loadPlanForDay = function(excInfo, weekNum){
		var oneRm = $scope.settings.info.oneRm[excInfo.id];
		var workingWeight = oneRm * .9;
		var assInfo = null;
		var assWeight = null;
		// assistance
		if ("bbb" == $scope.settings.assistance.variant) {
			assInfo = excInfo;
			var percent = $scope.settings.assistance.percents[excInfo.id] / 100;
			assWeight = Math.floor(percent * workingWeight);
		} else if ("lb" == $scope.settings.assistance.variant) {
			var percent = $scope.settings.assistance.percents[excInfo.alternate.id] / 100;
			var altOneRm = $scope.settings.info.oneRm[excInfo.alternate.id];
			assInfo = excInfo.alternate;
			assWeight = Math.floor(percent * altOneRm * .9);
		} else {
			throw "Unexpected program " + $scope.settings.assistance.variant;
		}

		var day = new WorkoutDay(excInfo);
        day.warmup = new WorkoutSetList(excInfo);
        day.primary = new WorkoutSetList(excInfo);
        day.assistance = new WorkoutSetList(assInfo);
        day.complement = new WorkoutSetList(excInfo.complement);


		// warmup
        $scope.settings.liftStatus[weekNum][excInfo.id]['warmup'][1]
        day.warmup.sets.push(new WorkoutSet(excInfo, 5,  Math.floor(workingWeight * .4), false, $scope.settings.liftStatus[weekNum][excInfo.id]['warmup'][1]));
        day.warmup.sets.push(new WorkoutSet(excInfo, 5,  Math.floor(workingWeight * .5), false, $scope.settings.liftStatus[weekNum][excInfo.id]['warmup'][2]));
        day.warmup.sets.push(new WorkoutSet(excInfo, 3,  Math.floor(workingWeight * .6), false, $scope.settings.liftStatus[weekNum][excInfo.id]['warmup'][3]));

        
        if (weekNum!=4){
            // primary
            var five31 = FIVE31_PLAN[weekNum - 1];
            var cntr = 0;
            for (cntr; cntr < five31.length; cntr++) {
                day.primary.sets.push(new WorkoutSet(excInfo, five31[cntr].reps, Math.floor(workingWeight * five31[cntr].ratio), five31[cntr].amrap, $scope.settings.liftStatus[weekNum][excInfo.id]['primary'][cntr+1]));
            }

            //assistance
    		for (cntr=0; cntr < 5; cntr++) {
                day.assistance.sets.push(new WorkoutSet(assInfo, 10,  assWeight, false, $scope.settings.liftStatus[weekNum][excInfo.id]['assistance'][cntr+1]));
    		}
        }
        

        //complement
		for (cntr = 0; cntr < excInfo.complement.sets; cntr++) {
            day.complement.sets.push(new WorkoutSet(excInfo.complement, excInfo.complement.reps, null, false, $scope.settings.liftStatus[weekNum][excInfo.id]['complement'][cntr+1]));
		}
		return day;
	};

	
	$scope.loadPlan = function(){
		if ($scope.plan!=null) return;
		console.log("Building plan");
		
		//TODO figure a smaller #? or who cares?
		SOLUTION_TABLE = new PlateTable(1800, $scope.plates[$scope.settings.info.units].bar, $scope.settings.plates[$scope.settings.info.units]);
		
		$scope.plateData = buildPlateInfo($scope);

		var workout = new Workout();
        var weekNum;
		for (weekNum=1; weekNum <= 4; weekNum++) {
            var week = new WorkoutWeek(weekNum);
			var cntr2 = 0;
			for (cntr2; cntr2 < $scope.exc.list.length; cntr2++) {
				var exc = $scope.exc.list[cntr2];
				var day = $scope.loadPlanForDay(exc, weekNum);
                week.days.push(day);
			}
            workout.weeks.push(week);
		}
        $scope.plan = workout;
        calcPlatesForWorkout($scope, $scope.plan);
    };
	
	$scope.loadSettings();
	$scope.plates = loadPlates();
	
	var autosave = $interval(function() {
		if ($scope.dirty){
			console.log("Autosaving...");
			$scope.cloudSave();
		}
      }, 5000);
	
} ]);