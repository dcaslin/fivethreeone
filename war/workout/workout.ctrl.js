function showLoadout($scope, label, setList, firstOnly) {
	// calcPlatesForSetList($scope, setList, firstOnly);
	$scope.plateModal = {
		label : label,
		setList : setList
	};
	$('#plateModal').modal();
};

angular.module('five31app').controller(
		"WorkoutController",
		[ '$interval', '$scope', 'WorkoutSet', 'WorkoutSetList', 'WorkoutDay', 'WorkoutWeek', 'Workout',
				function($interval, $scope, WorkoutSet, WorkoutSetList, WorkoutDay, WorkoutWeek, Workout) {

					$scope.loadPlan();
					$scope.excId = {
						1 : 'dl',
						2 : 'dl',
						3 : 'dl',
						4 : 'dl',
					};
					var sDays = localStorage.getItem("days");
					if (sDays) {
						$scope.excId = JSON.parse(sDays);
					}
					$scope.week = 1;
					var sWeek = localStorage.getItem("week");
					if (sWeek) {
						$scope.week = parseInt(sWeek);
					}

					$scope.setWeek = function(newValue) {
						$scope.week = newValue;
						localStorage.setItem("week", newValue);
					};

					$scope.isWeek = function(checkMe) {
						return checkMe == $scope.week;
					};

					$scope.setExc = function(weekNum, newValue) {
						$scope.excId[weekNum] = newValue;
						localStorage.setItem("days", JSON.stringify($scope.excId));
					};

					$scope.isDayDone = function(weekNum, excId) {
						if (weekNum == 4) {
							return $scope.settings.liftStatus[weekNum][excId]['warmup'][3] != "incomplete";
						}
						return $scope.settings.liftStatus[weekNum][excId]['primary'][3] != "incomplete";
					};

					$scope.isWeekDone = function(weekNum) {
						if (weekNum == 4) {
							if ($scope.settings.liftStatus[weekNum]['dl']['warmup'][3] == 'incomplete')
								return false;
							if ($scope.settings.liftStatus[weekNum]['sq']['warmup'][3] == 'incomplete')
								return false;
							if ($scope.settings.liftStatus[weekNum]['bp']['warmup'][3] == 'incomplete')
								return false;
							if ($scope.settings.liftStatus[weekNum]['ohp']['warmup'][3] == 'incomplete')
								return false;
							return true;
						}

						if ($scope.settings.liftStatus[weekNum]['dl']['primary'][3] == 'incomplete')
							return false;
						if ($scope.settings.liftStatus[weekNum]['sq']['primary'][3] == 'incomplete')
							return false;
						if ($scope.settings.liftStatus[weekNum]['bp']['primary'][3] == 'incomplete')
							return false;
						if ($scope.settings.liftStatus[weekNum]['ohp']['primary'][3] == 'incomplete')
							return false;
						return true;
					};

					$scope.isExc = function(weekNum, checkMe) {
						return checkMe == $scope.excId[weekNum];

					};

					$scope.showLoadout = function(label, setList, firstOnly) {
						showLoadout($scope, label, setList, firstOnly);
					};
					$scope.modal = {};

					$scope.updateModalOneRm = function() {
						$scope.modal.oneRm = $scope.oneRm($scope.modal.wSet.weight, $scope.modal.reps);
					}

					$scope.openModal = function(wSet) {
						$scope.modal.wSet = wSet;
						$('#workoutModal').modal();
					};

					$scope.printModal = function() {
						$('#printModal').modal();
					};

					$scope.applyModal = function() {
						$scope.addDiaryEntry(new Date(), $scope.week, $scope.modal.wSet.exc.id, $scope.modal.reps, $scope.modal.wSet.weight);
						if ($scope.modal.reps >= $scope.modal.wSet.reps) {
							$scope.modal.wSet.status = "complete";
						} else {
							$scope.modal.wSet.status = "miss";
						}

						$scope.settings.liftStatus[$scope.week][$scope.modal.wSet.exc.id]['primary'][3] = $scope.modal.wSet.status;
						$scope.save(false);

						$('#workoutModal').modal('hide');
					};

					$('#workoutModal').on('hidden.bs.modal', function(e) {
						console.log("hidden");
						$scope.oneRmForm.$setPristine();
						$scope.modal = {};
					});

					$scope.statusClasses = {
						'incomplete' : 'btn-default',
						'complete' : 'btn-success',
						'miss' : 'btn-danger'
					};

					$scope.excClick = function(cSet, weekNum, dayId, portion, setNum) {
						if ("incomplete" == cSet.status) {
							cSet.status = "complete";
						} else if ("complete" == cSet.status) {
							cSet.status = "miss";
						} else if ("miss" == cSet.status) {
							cSet.status = "incomplete";
						}
						$scope.settings.liftStatus[weekNum][dayId][portion][setNum] = cSet.status;
						$scope.save(false);
					};

					if (!$scope.settings.timerDuration) {
						$scope.settings.timerDuration = 120;
					}
					$scope.timer = {
						btnLabel : 'Start',
						started : false
					};

					$scope.timerModal = function() {
						$('#timerModal').modal();
						$scope.timer.seconds = $scope.settings.timerDuration;
						$scope.timerToggle(true);
						
					};
					$('#timerModal').on('hidden.bs.modal', function(e) {
						$scope.timerToggle(false, true);
					});
					$scope.updateDuration = function() {
						$scope.timer.seconds = $scope.settings.timerDuration;
						$scope.save(false);
					};
					var intPromise = null;
					$scope.timerToggle = function(forceStart, forceStop) {
						console.log('start');
						if ((!$scope.timer.started || forceStart)&& !forceStop) {
							if ($scope.timer.seconds == 0)
								$scope.timer.seconds = $scope.settings.timerDuration;

							$scope.timer.btnLabel = 'Stop';
							$scope.timer.started = true;
							intPromise = $interval(function() {
								$scope.timer.seconds--;
								if ($scope.timer.seconds == 0) {
									$interval.cancel(intPromise);
									$scope.timer.btnLabel = 'Start';
									$scope.timer.started = false;

									var audioElement = document.createElement('audio');
									audioElement.setAttribute('src', 'beep.mp3');
									audioElement.setAttribute('autoplay', 'autoplay');
									audioElement.addEventListener("load", function() {
										audioElement.play();
									}, true);

								}
							}, 1000);
						} else {
							if (forceStart) return;
							if (intPromise != null) {
								$interval.cancel(intPromise);
							}
							$scope.timer.btnLabel = 'Start';
							$scope.timer.started = false;
						}
					}
				} ]);