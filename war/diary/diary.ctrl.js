angular.module('five31app').controller(
//		
// str = JSURL.stringify(obj);
// obj = JSURL.parse(str);

    "DiaryController", ['$scope', function ($scope) {

        $scope.loadDiary = function () {
            $('#diaryLoadModal').modal();
        };

        $scope.csvDiary = {};

        $scope.loadDiary = function() {
            $('#diaryLoadModal').modal();
        };
        $scope.csvModal = {};

        $scope.buildCsv = function(){
            var csv = "Exc,Date,Week,Reps,Weight,1 RM\n";
            var cntr;
            for (cntr = 0; cntr < $scope.settings.diary.length; cntr++) {
                var entry = $scope.settings.diary[cntr];
                var sDate = moment(entry.date).format('MM/DD/YYYY');
                csv += entry.excId + "," + sDate + "," + entry.week + "," + entry.reps + "," + entry.weight + "," + entry.oneRm + "\n";
            }
            return csv;
        };
        $scope.viewCsv = function () {
            $scope.csvModal.csv = $scope.buildCsv();
            $('#viewCsvModal').modal();

        };
        $scope.downloadCsv = function(){
            var csv = $scope.buildCsv();
            var filename = "531diary_"+moment().format("MM-DD-YYYY")+".csv";
            var pom = document.createElement('a');
            pom.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv));
            pom.setAttribute('download', filename);
            pom.click();

        };

        $scope.sort = function(title){
            if ($scope.table.sort==title){
                $scope.table.reverse = !$scope.table.reverse;
            }
            else {
                $scope.table.sort = title;
                $scope.table.reverse = false;
            }
        };

        $scope.loadCsvDiary = function () {
            var parseMe = $scope.csvDiary.text;
            var lines = parseMe.split(/\r*\n/);
            var cntr = 0;
            var jEntries = [];
            $scope.csvDiary.msg = null;
            for (cntr; cntr < lines.length; cntr++) {

                var line = lines[cntr];
                line = line.trim();
                line = line.toLowerCase();
                if (line.startsWith("exc"))
                    continue;
                if (line.length == 0)
                    continue;
                var entries = line.split(",");
                if (entries.length < 5) {
                    $scope.csvDiary.msg = "Invalid input, line " + (cntr + 1) + " '" + line + "', must have at least 5 entries";
                    break;
                }
                switch (entries[0]) {
                    case "dl":
                        break;
                    case "sq":
                        break;
                    case "bp":
                        break;
                    case "ohp":
                        break;
                    default:
                        $scope.csvDiary.msg = "Invalid input, line " + (cntr + 1) + " '" + line + "', first entry must be one of dl/sq/bp/ohp";
                        break;
                }
                var date = moment(entries[1], 'M/D/YYYY', true);
                if (!date.isValid()) {
                    $scope.csvDiary.msg = "Invalid input, line " + (cntr + 1) + " '" + line + "', second entry must be date in format M/D/YYYY";
                    break;
                }
                var week = validateInt(entries[2], 1, 4);
                if (week == null) {
                    $scope.csvDiary.msg = "Invalid input, line " + (cntr + 1) + " '" + line + "', third entry must be 5/3/1 week, an integer between 1 and 4";
                    break;
                }
                var reps = validateInt(entries[3], 1, 100);
                if (reps == null) {
                    $scope.csvDiary.msg = "Invalid input, line " + (cntr + 1) + " '" + line + "', fourth entry must be reps, an integer between 1 and 100";
                    break;
                }
                var weight = validateFloat(entries[4], 0, 2000);
                if (weight == null) {
                    $scope.csvDiary.msg = "Invalid input, line " + (cntr + 1) + " '" + line + "', fifth entry must be weight, a float between 0 and 2000";
                    break;
                }
                var jEntry = {
                    "date": date.toDate(),
                    "week": week,
                    "excId": entries[0],
                    "reps": reps,
                    "weight": weight,
                    "oneRm": $scope.oneRm(weight, reps)
                };
                jEntries.push(jEntry);
            }
            if (!$scope.csvDiary.msg) {
                $scope.settings.diary = jEntries;
                $scope.save(false);
                $scope.csvDiary.msg = "Diary loaded!";
            }

        };


        var filterOpts = [{
            name: "All",
            id: ""
        }, {
            name: "Deadlift",
            id: "dl"
        }, {
            name: "Squat",
            id: "sq"
        }, {
            name: "Benchpress",
            id: "bp"
        }, {
            name: "Overhead Press",
            id: "ohp"
        }];

        $scope.table = {
            sort: "date",
            reverse: true,
            filter: filterOpts[0],
            filterOpts: filterOpts

        };
        $scope.remove = function (exc) {
            var loc = $scope.settings.diary.indexOf(exc);
            if (loc < 0)
                throw ("Exc not found");
            $scope.settings.diary.splice(loc, 1);
            $scope.save(false);
        };
        $scope.excOpts = [{
            name: "Deadlift",
            id: "dl"
        }, {
            name: "Squat",
            id: "sq"
        }, {
            name: "Benchpress",
            id: "bp"
        }, {
            name: "Overhead Press",
            id: "ohp"
        }];

        $scope.modal = {
            date: new Date(),
            week: 1,
            exc: $scope.excOpts[0]
        };

        $scope.openModal = function ($event) {
        	$('#addDate').data("datepicker").setDate($scope.modal.date);
            $('#addModal').modal();
        };
        $('#addModal').on('hidden.bs.modal', function (e) {
            console.log("hidden");
            $scope.addForm.$setPristine();
//            $scope.modal = {
//                week: 1,
//                exc: $scope.excOpts[0],
//            	date: new Date()
//            };
        });

        $scope.addRow = function () {
            console.log("hi");
            $scope.addDiaryEntry(moment($scope.modal.date).toDate(), $scope.modal.week, $scope.modal.exc.id, $scope.modal.reps, $scope.modal.weight);
            // $('#addModal').modal('hide');
        };

    }]);