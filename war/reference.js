function loadExc() {
	var dl = {
		name : "Deadlift",
		id : "dl",
		alternate : {
			id : "sq",
			name : "Squat"
		},
		complement : {
			id : "lr",
			name : "Hanging Leg Raise",
			sets : 5,
			reps : 15
		}
	};
	var sq = {
		name : "Squat",
		id : "sq",
		alternate: {
			id: "dl",
			name: "Deadlift"
		},
		complement : {
			id : "lc",
			name : "Leg Curl",
			sets : 5,
			reps : 10
		}
	};
	var ohp = {
		name : "Overhead Press",
		id : "ohp",
		alternate : {
			id : "bp",
			name : "Bench Press"
		},
		complement : {
			id : "cu",
			name : "Chinups",
			sets : 5,
			reps : 10
		}
	};
	var bp = {
		name : "Bench Press",
		id : "bp",
		alternate : {
			id : "ohp",
			name : "Overhead Press"
		},
		complement : {
			id : "kr",
			name : "Kroc Row",
			sets : 5,
			reps : 10
		}
	};

	var arExc = [ dl, sq, bp, ohp ];
	var refExc = {
		dl : dl,
		sq : sq,
		bp : bp,
		ohp : ohp
	};
	return {
		list : [ dl, sq, bp, ohp ],
		map : {
			dl : dl,
			sq : sq,
			bp : bp,
			ohp : ohp
		}
	};
}

function loadStandards() {

	var standards = {
		"dl" : {
			male : {
				standards : [ {
					'class' : 0,
					'cutoffs' : [ 95, 180, 205, 300, 385 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 105, 195, 220, 320, 415 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 115, 210, 240, 340, 440 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 125, 235, 270, 380, 480 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 135, 255, 295, 410, 520 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 150, 275, 315, 440, 550 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 155, 290, 335, 460, 565 ]
				}, {
					'class' : 220,
					'cutoffs' : [ 165, 305, 350, 480, 585 ]
				}, {
					'class' : 242,
					'cutoffs' : [ 170, 320, 365, 490, 595 ]
				}, {
					'class' : 275,
					'cutoffs' : [ 175, 325, 375, 500, 600 ]
				}, {
					'class' : 319,
					'cutoffs' : [ 180, 335, 380, 505, 610 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 185, 340, 390, 510, 615 ]
				} ]

			},
			female : {
				standards : [ {
					'class' : 0,
					'cutoffs' : [ 55, 105, 120, 175, 230 ]
				}, {
					'class' : 105,
					'cutoffs' : [ 60, 115, 130, 190, 240 ]
				}, {
					'class' : 114,
					'cutoffs' : [ 65, 120, 140, 200, 255 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 70, 130, 150, 210, 265 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 75, 135, 160, 220, 275 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 80, 150, 175, 240, 295 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 90, 160, 190, 260, 320 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 95, 175, 205, 275, 330 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 100, 185, 215, 285, 350 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 110, 195, 230, 300, 365 ]
				} ]
			}

		},
		"ohp" : {
			male : {
				standards : [ {
					'class' : 0,
					'cutoffs' : [ 55, 75, 90, 110, 130 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 60, 80, 100, 115, 140 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 65, 85, 105, 125, 150 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 70, 95, 120, 140, 170 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 75, 100, 130, 155, 190 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 80, 110, 140, 165, 220 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 85, 115, 145, 175, 235 ]
				}, {
					'class' : 220,
					'cutoffs' : [ 90, 120, 155, 185, 255 ]
				}, {
					'class' : 242,
					'cutoffs' : [ 95, 125, 160, 190, 265 ]
				}, {
					'class' : 275,
					'cutoffs' : [ 95, 130, 165, 195, 275 ]
				}, {
					'class' : 319,
					'cutoffs' : [ 100, 135, 170, 200, 280 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 100, 140, 175, 205, 285 ]
				} ]
			},
			female : {
				standards : [ {
					'class' : 0,
					'cutoffs' : [ 30, 40, 50, 65, 85 ]
				}, {
					'class' : 105,
					'cutoffs' : [ 35, 45, 55, 70, 90 ]
				}, {
					'class' : 114,
					'cutoffs' : [ 35, 50, 60, 75, 100 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 40, 50, 60, 80, 105 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 40, 55, 65, 85, 110 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 45, 60, 70, 95, 120 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 50, 65, 75, 105, 135 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 50, 70, 80, 110, 140 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 55, 75, 85, 115, 150 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 60, 80, 95, 125, 160 ]
				} ]
			}
		},
		"sq" : {
			male : {
				standards :

				[ {
					'class' : 0,
					'cutoffs' : [ 80, 145, 175, 240, 320 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 85, 155, 190, 260, 345 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 90, 170, 205, 280, 370 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 100, 190, 230, 315, 410 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 110, 205, 250, 340, 445 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 120, 220, 270, 370, 480 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 125, 230, 285, 390, 505 ]
				}, {
					'class' : 220,
					'cutoffs' : [ 130, 245, 300, 410, 530 ]
				}, {
					'class' : 242,
					'cutoffs' : [ 135, 255, 310, 425, 550 ]
				}, {
					'class' : 275,
					'cutoffs' : [ 140, 260, 320, 435, 570 ]
				}, {
					'class' : 319,
					'cutoffs' : [ 145, 270, 325, 445, 580 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 150, 275, 330, 455, 595 ]
				} ]
			},
			female : {
				standards : [ {
					'class' : 0,
					'cutoffs' : [ 45, 85, 100, 130, 165 ]
				}, {
					'class' : 105,
					'cutoffs' : [ 50, 90, 105, 140, 175 ]
				}, {
					'class' : 114,
					'cutoffs' : [ 55, 100, 115, 150, 190 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 55, 105, 120, 160, 200 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 60, 110, 130, 170, 210 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 65, 120, 140, 185, 230 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 70, 130, 150, 200, 255 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 75, 140, 165, 215, 270 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 80, 150, 175, 230, 290 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 85, 160, 185, 240, 305 ]
				} ]
			}
		},
		"bp" : {
			male : {
				standards : [

				{
					'class' : 0,
					'cutoffs' : [ 85, 110, 130, 180, 220 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 90, 115, 140, 195, 240 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 100, 125, 155, 210, 260 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 110, 140, 170, 235, 290 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 120, 150, 185, 255, 320 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 130, 165, 200, 275, 345 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 135, 175, 215, 290, 360 ]
				}, {
					'class' : 220,
					'cutoffs' : [ 140, 185, 225, 305, 380 ]
				}, {
					'class' : 242,
					'cutoffs' : [ 145, 190, 230, 315, 395 ]
				}, {
					'class' : 275,
					'cutoffs' : [ 150, 195, 240, 325, 405 ]
				}, {
					'class' : 319,
					'cutoffs' : [ 155, 200, 245, 335, 415 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 160, 205, 250, 340, 425 ]
				},

				]
			},
			female : {
				standards : [ {
					'class' : 0,
					'cutoffs' : [ 50, 65, 75, 95, 115 ]
				}, {
					'class' : 105,
					'cutoffs' : [ 55, 70, 80, 100, 125 ]
				}, {
					'class' : 114,
					'cutoffs' : [ 60, 75, 85, 110, 135 ]
				}, {
					'class' : 123,
					'cutoffs' : [ 65, 80, 90, 115, 140 ]
				}, {
					'class' : 132,
					'cutoffs' : [ 70, 85, 95, 125, 150 ]
				}, {
					'class' : 148,
					'cutoffs' : [ 75, 90, 105, 135, 165 ]
				}, {
					'class' : 165,
					'cutoffs' : [ 80, 95, 115, 145, 185 ]
				}, {
					'class' : 181,
					'cutoffs' : [ 85, 110, 120, 160, 195 ]
				}, {
					'class' : 198,
					'cutoffs' : [ 90, 115, 130, 165, 205 ]
				}, {
					'class' : 9999,
					'cutoffs' : [ 95, 120, 140, 175, 220 ]
				}

				]
			}
		}
	};
	return standards;
}

function loadPlates() {
	return {
		lbs : {
			bar: 45,
			common : [ {
				weight : 45,
				label : "45"
			}, {
				weight : 35,
				label : "35"
			}, {
				weight : 25,
				label : "25"
			}, {
				weight : 10,
				label : "10"
			}, {
				weight : 5,
				label : "5"
			}, {
				weight : 2.5,
				label : "2_5"
			},{
				weight : 1.25,
				label : "1_25"
			},{
				weight : 1.00,
				label : "1_00"
			},{
				weight : 0.75,
				label : "0_75"
			},{
				weight : 0.50,
				label : "0_50"
			},{
				weight : 0.25,
				label : "0_25"
			},

			]
		// ,rare : [ 100, 55, 2, 1.5, 1.25, 1, .75, .5, .25 ]
		},
		kg : {
			bar: 20,
			common : [ {
				weight : 25,
				label : "25"
			}, {
				weight : 20,
				label : "20"
			}, {
				weight : 15,
				label : "15"
			}, {
				weight : 10,
				label : "10"
			}, {
				weight : 5,
				label : "5"
			}, {
				weight : 2.5,
				label : "2_5"
			}, {
				weight : 1.25,
				label : "1_25"
			},{
				weight : 0.50,
				label : "_50"
			}  ]
//			,rare : [ 50, 2, 1, .75, .5, .25 ]
		}
	};
}

function doYouEvenLift(weight, units) {
	weight = normalizeWeightInLbs(weight, units);
	if (weight > 4000)
		return "Newborn Blue Whale";
	else if (weight > 3000)
		return "Black Rhinoceros";
	else if (weight > 2300)
		return "Water Buffalo";
	else if (weight > 1800)
		return "Clydesdale";
	else if (weight > 1000)
		return "Mature Male Moose";
	else if (weight > 800)
		return "Jersey Cow";
	else if (weight > 900)
		return "Musk Ox";
	else if (weight > 900)
		return "Hammerhead Shark";
	else if (weight > 800)
		return "Galapagos Tortoise";
	else if (weight > 700)
		return "Superior Eastern Highland Gorilla";
	else if (weight > 500)
		return "Polar Bear";
	else if (weight > 600)
		return "Stately American Elk";
	else if (weight > 500)
		return "Mature Bluefin Tuna";
	else if (weight > 400)
		return "West Indian Manatee";
	else if (weight > 380)
		return "Juvenile Crocodile";
	else if (weight > 360)
		return "Black Jaguar";
	else if (weight > 340)
		return "Newborn Orca Whale";
	else if (weight > 320)
		return "Mature Harbor Seal";
	else if (weight > 300)
		return "Sloth Bear";
	else if (weight > 280)
		return "Loggerhead Sea Turtle";
	else if (weight > 260)
		return "Fledgling Pacific Walrus";
	else if (weight > 240)
		return "Svelte Wildebeest";
	else if (weight > 220)
		return "Male Giant Panda";
	else if (weight > 200)
		return "Fledgling Hippo";
	else if (weight > 190)
		return "Baby Elephant";
	else if (weight > 180)
		return "Black Bear";
	else if (weight > 170)
		return "Male Red Kangaroo";
	else if (weight > 160)
		return "Mature Alpaca";
	else if (weight > 150)
		return "Medium-Sized Deer";
	else if (weight > 140)
		return "Komodo Dragon";
	else if (weight > 130)
		return "Baby Giraffe";
	else if (weight > 120)
		return "Desert Bighorn Ewe";
	else if (weight > 110)
		return "Young Male Puma";
	else if (weight > 100)
		return "Grey Wolf";
	else if (weight > 95)
		return "Cheetah";
	else if (weight > 90)
		return "Medium Warthog";
	else if (weight > 85)
		return "Spotted Serengeti Hyena";
	else if (weight > 80)
		return "Newborn Texas Longhorn";
	else if (weight > 75)
		return "Baby Zebra";
	else if (weight > 70)
		return "Small Mountain Goat";
	else if (weight > 65)
		return "Newborn Manatee";
	else if (weight > 60)
		return "Mature Wombat";
	else if (weight > 55)
		return "Giant Armadillo";
	else if (weight > 50)
		return "Female Emperor Penguin";
	else if (weight > 45)
		return "Young Alpine Ibex";
	else if (weight > 40)
		return "Clouded Leopard";
	else if (weight > 35)
		return "Moose Calf";
	else if (weight > 30)
		return "Baby Sea Lion";
	else if (weight > 25)
		return "Stately Iberian Lynx";
	else if (weight > 20)
		return "Wolverine";
	else if (weight > 15)
		return "River Otter";
	else if (weight > 10)
		return "Red Fox";
	else if (weight > 5)
		return "European Hare";
	else if (weight > 1)
		return "Platypus";
	else if (weight > 0)
		return "Roberto's California Burrito";
	else
		return "Nothing";
}

function kgToLbs(kg){
	return kg*2.20462;
}

function lbToKg(lb){
	return lb/ 2.20462;
} 

function normalizeWeightInLbs(weight, units) {
	if (units == "kg") {
		weight = weight * 2.20462;
	}
	return weight;
}

var FIVE31_PLAN = [ [ {
	ratio : .65,
	reps : 5
}, {
	ratio : .75,
	reps : 5
}, {
	ratio : .85,
	reps : 5, 
	amrap: true
} ], [ {
	ratio : .7,
	reps : 3
}, {
	ratio : .8,
	reps : 3
}, {
	ratio : .9,
	reps : 3,
	amrap: true
} ], [ {
	ratio : .75,
	reps : 5
}, {
	ratio : .85,
	reps : 3
}, {
	ratio : .95,
	reps : 1, 
	amrap: true
} ], [ {
	ratio : .4,
	reps : 5
}, {
	ratio : .5,
	reps : 5
}, {
	ratio : .6,
	reps : 5
} ] ];

function getDefaultLiftStatus(){
	var liftStatus = {
			1:{
				'dl':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
				},
				'sq':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'bp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'ohp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				}
			},
			2:{
				'dl':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
				},
				'sq':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'bp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'ohp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				}			
			},
			3:{
				'dl':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
				},
				'sq':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'bp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'ohp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'primary':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'assistance':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				}				
			},
			4:{
				'dl':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					},
				},
				'sq':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'bp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				},
				'ohp':{
					'warmup':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete'
					},
					'complement':{
						1:'incomplete',
						2:'incomplete',
						3:'incomplete',
						4:'incomplete',
						5:'incomplete'
					}
				}				
			}
	};
	return liftStatus;
}

function getDefault() {
	return {
		info : {
			units : "lbs",
			sex : "male",
			weight : 0,
			oneRm : {
				dl : 0,
				sq : 0,
				bp : 0,
				ohp : 0
			}
		},
		plates : {
			lbs : null,
			kg : null
		},
		assistance : {
			variant : "lb",
			percents : {
				dl : 50,
				sq : 50,
				bp : 50,
				ohp : 50
			}
		},
		liftStatus: getDefaultLiftStatus(),
		diary : []
	};

}
