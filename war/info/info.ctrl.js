function selectStandard(weight, excStandards) {
	var cntr = 0;
	for (cntr; cntr < excStandards.standards.length; cntr++) {
		if (weight <= excStandards.standards[cntr].class) {
			return excStandards.standards[cntr].cutoffs;
		}
	}
}

function doYouEvenShow(weight, units) {
	if (!weight)
		return;
	if (weight < 1)
		return;
	console.log(weight + " " + units);
	$("#doYouEvenWeight").val(weight);
	$("#doYouEvenUnits").val(units);
	$("#doYouEven").submit();

}

function setupChart(cs) {
	var weight = normalizeWeightInLbs(cs.settings.info.weight, cs.settings.info.units);
	if (!weight)
		weight = 999;
	var selectedExcStandards = [];

	var cntr = 0;
	for (cntr; cntr < cs.exc.list.length; cntr++) {
		var current = cs.exc.list[cntr];

		var standards = selectStandard(weight, cs.standards[current.id][cs.settings.info.sex]);
		var newItem = {
			"exc" : cs.exc.list[cntr],
			"standards" : standards
		};
		selectedExcStandards.push(newItem);
	}
	cs.chart = selectedExcStandards;
}

angular.module('five31app').controller("InfoController", [ '$scope', function($scope) {
	$scope.standards = loadStandards();
	$scope.doYouEvenLift = doYouEvenLift;
	$scope.doYouEvenShow = doYouEvenShow;
	$scope.modal = {};

	$(".alert button.close").click(function(e) {
		$(this).parent().fadeOut('slow');
	});
	
	$scope.updateModalOneRm = function(){
		$scope.modal.oneRm = $scope.oneRm($scope.modal.weight, $scope.modal.reps);
	}

	$scope.openModal = function($event, exc) {
		$scope.modal.exc = exc;
		$('#myModal').modal();
	};
	$scope.applyModal = function() {
		var val = $scope.oneRm($scope.modal.weight, $scope.modal.reps);
		$scope.settings.info.oneRm[$scope.modal.exc.id] = val;
		$scope.modChart();
		$('#myModal').modal('hide');
	};
	$scope.levelUp = function() {
		$scope.settings.info.oneRm['dl'] += 10;
		$scope.settings.info.oneRm['sq'] += 10;
		$scope.settings.info.oneRm['bp'] += 5;
		$scope.settings.info.oneRm['ohp'] += 5;
		$scope.modChart();
	};
	$scope.levelDown = function() {
		$scope.settings.info.oneRm['dl'] -= 10;
		$scope.settings.info.oneRm['sq'] -= 10;
		$scope.settings.info.oneRm['bp'] -= 5;
		$scope.settings.info.oneRm['ohp'] -= 5;
		$scope.modChart();
	};
	setupChart($scope);

	$scope.modChart = function() {
		setupChart($scope);
		$scope.save(true);
	};
	
	$scope.changeUnits = function(unit){
		console.log(unit);
		//translate weight, DL/SQ/BP/OHP
		if ('kg'==unit){
			$scope.settings.info.oneRm['dl'] = Math.round(lbToKg($scope.settings.info.oneRm['dl']));
			$scope.settings.info.oneRm['sq'] = Math.round(lbToKg($scope.settings.info.oneRm['sq']));
			$scope.settings.info.oneRm['bp'] = Math.round(lbToKg($scope.settings.info.oneRm['bp']));
			$scope.settings.info.oneRm['ohp'] = Math.round(lbToKg($scope.settings.info.oneRm['ohp']));
			$scope.settings.info.weight = Math.round(lbToKg($scope.settings.info.weight));
		}else{
			$scope.settings.info.oneRm['dl'] = Math.round(kgToLbs($scope.settings.info.oneRm['dl']));
			$scope.settings.info.oneRm['sq'] = Math.round(kgToLbs($scope.settings.info.oneRm['sq']));
			$scope.settings.info.oneRm['bp'] = Math.round(kgToLbs($scope.settings.info.oneRm['bp']));
			$scope.settings.info.oneRm['ohp'] = Math.round(kgToLbs($scope.settings.info.oneRm['ohp']));
			$scope.settings.info.weight = Math.round(kgToLbs($scope.settings.info.weight));
		}
		
		$scope.modChart();
	};
	$scope.norm = function(weight){
		if ("kg"==$scope.settings.info.units){
			return Math.round(lbToKg(weight));
		}
		return weight;
	};
	
	$('#myModal').on('hidden.bs.modal', function(e) {
		console.log("hidden");
		$scope.oneRmForm.$setPristine();
		$scope.modal = {};
	});

	$scope.$watch('modal.weight', function(newValue, oldValue) {
		if (newValue && $scope.modal.reps){
			$scope.modal.oneRm = $scope.oneRm($scope.modal.weight, $scope.modal.reps);
		}
	});
} ]);