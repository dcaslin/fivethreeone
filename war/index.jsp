<!DOCTYPE html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>5/3/1 Calc</title>

<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">

<meta name="description"
	content="Calculate your 1RM, your strength standards and a 5/3/1 program all in one place. Then track your workouts with a built-in diary. Great for bodybuilders, powerlifters, or just anyone working out.">

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
        google.load('visualization', '1.0', {'packages': ['corechart']});
    </script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link id="csstheme" rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker3.min.css">
<link href="vendor/css/bootstrap-slider.css" rel="stylesheet">
<link rel="stylesheet" href="app.css">

</head>
<body ng-app="five31app" ng-controller="MainController as main" ng-strict-di>
	<div class="container-fluid">
		<div ng-include="'header.html'"></div>
		<div style="display: none">
			<form id="doYouEven" name="doYoueven" method="POST" action="http://whatanimaldoyoulift.herokuapp.com/" target="_blank">
				<input id="doYouEvenWeight" type="hidden" name="weight" /> <input id="doYouEvenUnits" type="hidden" name="units" />
			</form>
		</div>

		<div class="page-loading hidden">Loading...</div>
		<div class="row">


			<div id="#center" class="col-sm-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="#/intro">Start</a></li>
					<li><a href="#/info">Info</a></li>
					<li><a href="#/plate">Plates</a></li>
					<li><a href="#/assistance">Assistance</a></li>
					<li><a href="#/workout">Workout</a></li>
					<li><a href="#/diary">Diary</a></li>
					<li><a href="#/chart">Chart</a></li>
				</ol>
				<div class="" ng-view=""></div>

			</div>

			<div class="abp abp-lg col-sm-4 visible-lg-block visible-md-block">
				<script type="text/javascript">
                google_ad_client = "ca-pub-4577479845324857";
                google_ad_slot = "8612874123";
                google_ad_width = 300;
                google_ad_height = 600;
            </script>
				<!-- LargeSky -->
				<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js">

            </script>

			</div>
			<div class="abp abp-med col-sm-4 visible-sm-block">
				<script type="text/javascript">
                google_ad_client = "ca-pub-4577479845324857";
                google_ad_slot = "8288143326";
                google_ad_width = 160;
                google_ad_height = 600;
            </script>
				<!-- WideSky -->
				<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js">

            </script>
			</div>

		</div>

		<br />

		<div class="abp abp-sm hidden-xs textCenter">
			
					<div class="alignleft">  
						<script type='text/javascript'>
						 amzn_assoc_ad_type = 'banner';
						 amzn_assoc_tracking_id = '531calccom-20';
						 amzn_assoc_marketplace = 'amazon';
						 amzn_assoc_region = 'US';
						 amzn_assoc_placement = 'assoc_banner_placement_default';
						 amzn_assoc_linkid = 'CBKBCIRC6VXTV2YZ';
						 amzn_assoc_campaigns = 'sports';
						 amzn_assoc_p = '48';
						 amzn_assoc_banner_type = 'category';
						 amzn_assoc_isresponsive = 'false';
						 amzn_assoc_banner_id = '04XMCHDMTG0EWW8WWXG2';
						 amzn_assoc_width = '728';
						 amzn_assoc_height = '90';
						</script>
						<script src='//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1'></script>
					</div>			
			
		</div>

	
		
		</div>

		<div class="abp abp-sm visible-xs-block small textCenter">
		
			<%
			int val = new java.util.Random().nextInt(2);
			if (val==1){
			%>
		<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=531calccom-20&o=1&p=12&l=ur1&category=sports&banner=16JZ3TQKM4XZ0M64QS02&f=ifr&linkID=MCYOIAFS33QRS4BP" width="300" height="250" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		 	
			<%
			}
			else{
			%>
			<script type="text/javascript">
	            google_ad_client = "ca-pub-4577479845324857";
	            google_ad_slot = "4024994522";
	            google_ad_width = 320;
	            google_ad_height = 100;
        	</script>
			<script type="text/javascript" src="//pagead2.googlesyndication.com/pagead/show_ads.js"></script>
			<%
			}
			%>
		
			
		</div>

		<div ng-include="'footer.html'"></div>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-route.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/js/bootstrap-datepicker.js"></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>

	<script src="vendor/js/bootstrap-slider.js"></script>
	<script src="vendor/js/lz-string.min.js"></script>
	<script src="vendor/js/slider.js"></script>
	<script src="external.js"></script>
	<script src="base.js"></script>
	<script src="ads.js"></script>


	<script src="reference.js"></script>
	<script src="plates.js"></script>

	<script src="app.js"></script>
	<script src="main.ctrl.js"></script>
	<script src="assistance/assistance.ctrl.js"></script>
	<script src="diary/diary.ctrl.js"></script>
	<script src="info/info.ctrl.js"></script>
	<script src="plate/plate.ctrl.js"></script>
	<script src="workout/workout.ctrl.js"></script>
	<script src="chart/chart.ctrl.js"></script>
	<script src="about/about.ctrl.js"></script>
	<script src="intro/intro.ctrl.js"></script>
	<script src="equip/equip.ctrl.js"></script>

</body>
</html>
