angular.module('five31app').controller("AssistanceController",
		[ '$scope', function($scope) {

			$scope.programLevelDesc = function(percent) {
				if (percent <= 40)
					return "Easy";
				else if (percent<=69)
					return "Medium";
				else
					return "Hard";
			};
			$scope.showActualWeight = function(exc){
				var val = $scope.settings.assistance.percents[exc.id] *.01 * .9 * $scope.settings.info.oneRm[exc.id];
				return Math.floor(val);
			}
		} ]);