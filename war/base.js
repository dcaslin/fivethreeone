'use strict';

if (typeof String.prototype.endsWith != 'function') {
	String.prototype.endsWith = function(suffix) {
		return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

if (typeof String.prototype.startsWith != 'function') {
	// see below for better implementation!
	String.prototype.startsWith = function(str) {
		return this.indexOf(str) === 0;
	};
}

(function($) {
	$.querystring = (function(a) {
		var i, p, b = {};
		if (a === "") {
			return {};
		}
		for (i = 0; i < a.length; i += 1) {
			p = a[i].split('=');
			if (p.length === 2) {
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}
		}
		return b;
	}(window.location.search.substr(1).split('&')));
}(jQuery));

function isInt(n) {
	return n % 1 === 0;
}

function sortNumber(a, b) {
	return a - b;
}

function reverseSortNumber(a, b) {
	return b - a;
}

function validateInt(value, min, max) {
	if (/^(\-|\+)?([0-9]+|Infinity)$/.test(value)) {
		var n = Number(value);
		if (min) {
			if (n < min)
				return null;
		}
		if (max) {
			if (n > max)
				return null;
		}
		return n;
	}

	return null;
}

function validateFloat(value, min, max) {
	if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
		var n = Number(value);
		if (min) {
			if (n < min)
				return null;
		}
		if (max) {
			if (n > max)
				return null;
		}
		return n;
	}
	return null;
}

function getAbsUrl() {
	var url = window.location.href;
	if (url.indexOf('#') > 0)
		url = url.substring(0, url.indexOf('#'));
	if (url.indexOf('?') > 0)
		url = url.substring(0, url.indexOf('?'));
	return url;
}


function queryStringToJSON() {
	var pairs = location.search.slice(1).split('&');

	var result = {};
	pairs.forEach(function(pair) {
		pair = pair.split('=');
		result[pair[0]] = decodeURIComponent(pair[1] || '');
	});

	return JSON.parse(JSON.stringify(result));
}

function getStorage(key, defVal) {
	var item = localStorage.getItem(key);
	if (!item)
		return defVal;
	if (isInt(defVal))
		return parseInt(item);
	return item;
}

var ADS_WORKING = false;

function fillinAds() {
	if (!ADS_WORKING) {
		$("div.abp").addClass("adblock");

		$("div.abp-lg")
				.html(
						'Advertising seems to be blocked in your browser. If you find this site helpful, please consider whitelisting it.<br><img src="abp.png" alt="Adblock Plus"><br/> <a class="abplink" href="about.html">Click here for more info</a>');
		$("div.abp-lg").addClass("adblock-lg");
		$("div.abp-med")
				.html(
						'Advertising seems to be blocked in your browser. If you find this site helpful, please consider whitelisting it.<br><img src="abp.png" alt="Adblock Plus"><br/> <a class="abplink" href="about.html">Click here for more info</a>');
		$("div.abp-med").addClass("adblock-med");

		$("div.abp-sm").addClass("adblock-sm");
		$("div.abp-sm")
				.html(
						'<div class="adblock-inner-sm">Advertising seems to be blocked in your browser. Please consider whitelisting this site.<br/> <a class="abplink" href="about.html">Click here for more info</a></div>');
	}
}

$(document).ready(fillinAds);