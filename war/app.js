'use strict';

var five31app = angular.module('five31app', [ 'ngRoute', 'ui.bootstrap-slider' ]);

five31app.config([ '$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/intro', {
		templateUrl : 'intro/intro.html',
		controller : 'IntroController'
	}).when('/equip', {
		templateUrl : 'equip/equip.html',
		controller : 'EquipController'
	}).when('/info', {
		templateUrl : 'info/info.html',
		controller : 'InfoController'
	}).when('/plate', {
		templateUrl : 'plate/plate.html',
		controller : 'PlateController'
	}).when('/assistance', {
		templateUrl : 'assistance/assistance.html',
		controller : 'AssistanceController'
	}).when('/workout', {
		templateUrl : 'workout/workout.html',
		controller : 'WorkoutController'
	}).when('/diary', {
		templateUrl : 'diary/diary.html',
		controller : 'DiaryController'
	}).when('/chart', {
		templateUrl : 'chart/chart.html',
		controller : 'ChartController'
	}).when('/about', {
		templateUrl : 'about/about.html',
		controller : 'AboutController'
	}).when('/privacy', {
		templateUrl : 'privacy/privacy.html',
		controller : 'PrivacyController'
	}).otherwise({
		redirectTo : '/intro'
	});
} ]);


five31app.filter('currentdate',['$filter',  function($filter) {
    return function() {
        return $filter('date')(new Date(), 'yyyy-MM-dd');
    };
}]);


five31app.filter('eggtimer',['$filter',  function($filter) {
    return function(input) {
    	var mins = Math.floor(input/60);
    	var secs = input % 60;
    	if (mins>0)
    		return mins+"m "+secs+"s";
    	return secs+"s";
    };
}]);

five31app.directive('datetimez', function() {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, element, attrs, ngModelCtrl) {
			element.datepicker({
				// defaults
				startDate : "01/01/2000",
				endDate : "01/01/2030",
				startView : 1,
				todayBtn : true,
				autoclose : true,
				todayHighlight : true

			}).on('changeDate', function(e) {
				ngModelCtrl.$setViewValue(e.date);
				if(!scope.$$phase) {
					  //$digest or $apply
					scope.$apply();
				}
				
			});
		}
	};
});


five31app.directive('printDiv', function () {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind('click', function(evt){    
        evt.preventDefault();    
        PrintElem(attrs.printDiv);
      });

      function PrintElem(elem)
      {
        PrintWithIframe($(elem).html());
      }

      function PrintWithIframe(data) 
      {
        if ($('iframe#printf').size() == 0) {
          $('html').append('<iframe id="printf" name="printf"></iframe>');  // an iFrame is added to the html content, then your div's contents are added to it and the iFrame's content is printed

          var mywindow = window.frames["printf"];
          var s = '<html><head><title></title>'
	  		  //+'<link rel="stylesheet" type="text/css" href="print2.css">'
              + '</head><body><div class="printContainer">'
              + data
              + '</div></body></html>';
          mywindow.document.write(s);

          $(mywindow.document).ready(function(){
            mywindow.print();
            setTimeout(function(){
              $('iframe#printf').remove();
            },
            2000);  // The iFrame is removed 2 seconds after print() is executed, which is enough for me, but you can play around with the value
          });
        }

        return true;
      }
    }
  };
});

five31app.factory('WorkoutSet', function() {
	function WorkoutSet(exc, reps, idealWeight, amrap, status) {
		this.exc = exc;
		this.reps = reps;
		this.idealWeight = idealWeight;
		this.weight = this.idealWeight;
		this.plates = null;
		this.status = status; //incomplete/complete/miss
		this.miss = false;
		if (amrap)
			this.amrap = true;
		else
			this.amrap = false;
	}

	WorkoutSet.prototype.getDesc = function() {
		return this.excId + ' ' + this.reps;
	};
	return (WorkoutSet);
});

five31app.factory('WorkoutSetList', function() {
	function WorkoutSetList(exc) {
		this.exc = exc;
		// array of WorkoutSets
		this.sets = [];
	}

	WorkoutSetList.prototype.getDesc = function() {
		return this.exc.id + ' ' + this.sets.length;
	};
	return (WorkoutSetList);
});

five31app.factory('WorkoutDay', function() {
	function WorkoutDay(exc) {
		this.exc = exc;
		// WorkoutSetList
		this.warmup = null;
		this.primary = null;
		this.assistance = null;
		this.complement = null;
	}

	WorkoutDay.prototype.getDesc = function() {
		return this.exc.id;
	};
	return (WorkoutDay);
});

five31app.factory('WorkoutWeek', function() {
	function WorkoutWeek(weekNum, days) {
		// array of WorkoutDays
		this.weekNum = weekNum;
		if (days == null)
			days = [];
		this.days = days;
	}

	WorkoutWeek.prototype.getDesc = function() {
		return this.weekNum;
	};
	return (WorkoutWeek);
});

five31app.factory('Workout', function() {
	function Workout(weeks) {
		// array of WorkoutWeek
		if (weeks == null)
			weeks = [];
		this.weeks = weeks;
	}

	Workout.prototype.getDesc = function() {
		return this.weeks.length;
	};
	return (Workout);
});

five31app.service("UserService", ["$http", "$q", function($http, $q) {
	// Return public API.
	return ({
		getLogonUrl : getLogonUrl,
		getLogoffUrl : getLogoffUrl,
		isLoggedOn : isLoggedOn,
		getUserData : getUserData,
		setUserData : setUserData
	});
	
	function getLogonUrl() {
		var request = $http({
			method : "get",
			url : "Api",
			params : {
				action : "getlogonurl"
			}
		});
		return (request.then(function(response){
			console.log(response.data.status+": "+response.data.data);
			if ("SUCCESS"==response.data.status)
				return response.data.data;
		}, handleError));
	}
	
	function getLogoffUrl() {
		var request = $http({
			method : "get",
			url : "Api",
			params : {
				action : "getlogoffurl"
			}
		});
		return (request.then(function(response){
			console.log(response.data.status+": "+response.data.data);
			if ("SUCCESS"==response.data.status)
				return response.data.data;
		}, handleError));
	}
	
	function isLoggedOn() {
		var request = $http({
			method : "get",
			url : "Api",
			params : {
				action : "isloggedon"
			}
		});
		return (request.then(function(response){
			console.log(response.data.status+": "+response.data.data);
			if ("LOGGEDIN"==response.data.status)
				return response.data.data;
			else if ("NOTLOGGEDIN"==response.data.status)
				return null;
			else 
				throw "Unexpected response: "+response.data.status;
		}, handleError));
	}

	function getUserData() {
		var request = $http({
			method : "get",
			url : "Api",
			params : {
				action : "getuserdata"
			}
		});
		return (request.then(function(response){
			if ("SUCCESS"==response.data.status)
				return response.data.data;
			else 
				return null;
		}, handleError));
	}
	
	function setUserData(userData) {
		var request = $http({
			method : "post",
			url : "Api",
			params : {
				action : "setuserdata"
			},
			data:{
				data: userData
			}
		});
		return (request.then(function(response){
			if ("SUCCESS"==response.data.status)
				return response.data.data;
			else 
				return null;
		}, handleError));
	}
	
	function handleError(response) {
		if (!angular.isObject(response.data) || !response.data.message) {
			return ($q.reject("An unknown error occurred."));
		}
		return ($q.reject(response.data));
	}

	function handleLogonUrlSuccess(response){
		console.log(response.data);
	}
	
	function handleSuccess(response) {
		return (response.data);
	}

}]);