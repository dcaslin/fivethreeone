package caslin.five31.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import caslin.five31.model.UserData;

import com.googlecode.objectify.ObjectifyService;

public class LoadHelper implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

		System.out.println("531 shutting down.");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("531 starting up.");
	    ObjectifyService.register(UserData.class);

	}

}
