package caslin.five31.web;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import caslin.five31.model.Response;
import caslin.five31.model.UserData;

import com.google.api.server.spi.IoUtil;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.googlecode.objectify.ObjectifyService;

@SuppressWarnings("serial")
public class Api extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doGet(req, resp);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String action = req.getParameter("action");
		if (action == null || action.trim().length() == 0)
			throw new RuntimeException("action must be specified");
		Response r = new Response();
		if ("getlogonurl".equals(action)) {
			UserService userService = UserServiceFactory.getUserService();
			if (req.getUserPrincipal() != null) {
				r.status = "LOGGEDIN";
				r.data = req.getUserPrincipal().getName();
			} else {
				r.status = "SUCCESS";
				r.data = userService.createLoginURL("/index.jsp");
			}
		} else if ("getlogoffurl".equals(action)) {
			UserService userService = UserServiceFactory.getUserService();
			if (req.getUserPrincipal() == null) {
				r.status = "NOTLOGGEDIN";
			} else {
				r.status = "SUCCESS";
				r.data = userService.createLogoutURL("/index.jsp");
			}
		} else if ("isloggedon".equals(action)) {
			if (req.getUserPrincipal() != null) {
				r.status = "LOGGEDIN";
				r.data = req.getUserPrincipal().getName();
			}
			else{
				r.status = "NOTLOGGEDIN";
				
			}
		} 
		else if ("getuserdata".equals(action)){
			if (req.getUserPrincipal() == null) {
				r.status = "NOTLOGGEDIN";
			} else {
				UserData d = ObjectifyService.ofy().load().type(UserData.class).id(req.getUserPrincipal().getName()).get();
				if (d==null){
					r.status = "NOTFOUND";
					r.data = req.getUserPrincipal();
				}
				else{
					r.status = "SUCCESS";
					r.data = d.jsonData;
				}
			}
		}else if ("setuserdata".equals(action)){
			if (req.getUserPrincipal() == null) {
				r.status = "NOTLOGGEDIN";
			} else {
				UserData u = ObjectifyService.ofy().load().type(UserData.class).id(req.getUserPrincipal().getName()).get();
				String data = IoUtil.readStream(req.getInputStream());
				
//				String data = req.getParameter("data");
				if (data == null || data.length()==0) throw new RuntimeException("Bad request.");
				
				boolean isNew = false;
				if (u==null){
					u = new UserData();
					u.email = req.getUserPrincipal().getName();
					u.createDate = new Date();
					isNew = true;
				}
				u.updateDate = new Date();
				u.jsonData = data;
				ObjectifyService.ofy().save().entity(u).now();
				
				r.status = "SUCCESS";
				if (isNew)
					r.data = "ADDED";
				else
					r.data = "UPDATED";
			}
		}
		else {
			r.status = "UNKNOWNCOMMAND";
			r.data = action + " is unknown";
		}
		
		String json = new Gson().toJson(r);
		resp.setContentType("application/json");
		resp.getWriter().write(json);		
	}
}
