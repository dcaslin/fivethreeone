package caslin.five31.model;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class UserData {
	  @Id public String email;
	  public String jsonData;
	  public Date createDate;
	  public Date updateDate;
}
